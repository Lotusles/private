(function($){

    var BlogTitle = $(".top h3");
    var Link = $(".top a");
    var thumbsContainer = $('.moustache > .thumb');
    var images = ['http://dlewis.net/nik-archives/wp-content/uploads/2011/12/Thumbs-Up.png', 'http://blog.room34.com/wp-content/uploads/underdog/logo.thumbnail.png', 'https://lh3.googleusercontent.com/5EfQBHDb47tchiART6U6yk3yYS9qBYr6VUssB5wHE1AgavqV5E2SSuzyiNkc7UgVng=w300'];
    var index = 1;
    var contact = $(".contact-us a");

    var body = $(".page-template-homepage");


/**********************************************************************************************************************/

    $( document ).ready(function() {

        setTimeout(changeAnimation,5000);
        thumbsContainer.prepend('<img src="http://dlewis.net/nik-archives/wp-content/uploads/2011/12/Thumbs-Up.png" id="slider" style="width: 100%; height: auto" />');
        setInterval (rotateImage, 2500);

        popup();

        zoomAnimation();

    });

/**********************************************************************************************************************/

    function popup(){
        contact.click(function(e){
            e.preventDefault();
            body.prepend("<div id='dialog' title='Contact Gegevens:'><p><strong>AppSaloon bvba </strong><br> Bampslaan 21 3.01 <br> 3500 Hasselt <br> BELGIUM <br> Phone: <a href='tel:+3211123589'>+32 11 123 589</a></p></div>");
            $("#dialog").dialog();
        });
    }

    function zoomAnimation(){

        $('#content > section').each(function(i) {
           delay =(i)*500;

           $(this).delay(delay).animate({
               zoom:1.1
            }, {
            duration: 500,
            complete: function() {
               $(this).css('zoom', '1');
            }
          });
        });
    }


    function rotateImage()
    {
        $('#slider').fadeOut('fast', function()
        {
            $(this).attr('src', images[index]);

            $(this).fadeIn('fast', function()
            {
                if (index === images.length-1)
                {
                    index = 0;
                }
                else
                {
                    index++;
                }
            });
        });
    }

    function changeAnimation(){
        changeTarget(Link,"https://appsaloon.be/about/");
        changeBlogTitle();
    }

    function changeBlogTitle(){
        BlogTitle.html("Meet our team");
    }

    function changeTarget(Selector,target){
        Selector.attr("href", target)
    }

})(jQuery);

