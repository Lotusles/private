(function($){

    $(document).ready(function(){
        stopGame();

        start.on('click', startGame);
        stop.on('click', stopGame);

        //$('.user_input').on('click', playerinput);
        $('.user_input').mousedown(playerinput);
    });

    var rotate, t, generatedNumber, generatedOrder = [], userorder = [], lastEntry,
        images = ["img/img1.jpg","img/img2.jpg","img/img3.jpg","img/img4.jpg","img/img5.jpg"];

    var image = document.getElementById("light"),
        timer1 = document.getElementById('gametime'),
        timer2 = document.getElementById('lastround'),
        rounds = document.getElementById('round'),
        lastGame = document.getElementById('lastgame'),
        stepBar = document.getElementById('step-bar'),
        start = $('#start'),
        stop = $('#stop');

    var layer = $('.layer'),
        playing = false,
        spinning = false,
        currentPos = 0,
        seconds = 0,
        minutes = 0,
        hours = 0,
        round = 0,
        step = 0,
        clickcount = 0;

    var u = 1000;

    // Gameplay
    function startGame(){
        if (playing){
            stopGame();
        }else{
            init();
            startRound();
            noLight();
            //startRound(2);
        }
    }

    function init(){
        playing = true;
        stopRotate();
        layer.removeClass('show');
        addRounds();
        timer();
        resetStatusBar();
    }

    function stopGame(){
        if (playing){
            playing = false;
            endGame();
            clearUserArray();
            clearArray();
            layer.addClass('show');
            //clearArray();
        }else{
            if (!spinning){
                startSpin();
            }
            layer.addClass('show');
        }
    }

    function endGame(){
        clearTimeout(t);
        updateLastRound();
        updateLastGame();
        startSpin();
    }

    function startRound(){
        resetStatusBar();
        generateNumber();

        var arraycount = generatedOrder.length;

        jQuery.each( generatedOrder, function( i, val ) {
            setTimeout(function(){
                image.src = images[val];
                increaseStatusBar(arraycount);
            }, u);

            u += 500;

            setTimeout(function(){
                image.src = images[0];
            }, u);
        });

        u = 1000;
    }

    // Number generator
    function generateNumber() {

        addArray(generatedNumber = (Math.floor(Math.random() * 4) + 1));
    }

    function addArray(number){

        lastEntry = generatedOrder[generatedOrder.length-1];

        if (number === lastEntry){
            number = (Math.floor(Math.random() * 4) + 1);
        }

        generatedOrder.push(number);
    }

    function clearArray(){
        generatedOrder = [];
    }

    function clearUserArray(){
        userorder = [];
    }

    // Statusbar
    function resetStatusBar(){
        step = 0;
        stepBar.style.width = "0%";
    }

    function increaseStatusBar(amount) {
        step += (100 / amount);
        stepBar.style.width = step + "%";
    }

    // Spinner Functions
    function startSpin(){
        spinning = true;
        return rotate = setInterval(Rotate, 500);
    }

    function stopRotate(){
        clearInterval(rotate);
        image.src = images[0];
        spinning = false;
        return currentPos = 0;
    }

    function Rotate(){
        if (++currentPos >= images.length){
            currentPos = 0;
        }
        image.src = images[currentPos];
    }

    function showLight(number) {

        image.src = images[number];
    }

    function noLight(){
        image.src = images[0];
    }

    //Stopwatch
    function add() {
        seconds++;
        if (seconds >= 60) {
            seconds = 0;
            minutes++;
            if (minutes >= 60) {
                minutes = 0;
                hours++;
            }
        }

        timer1.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
        timer();
    }

    function timer() {
        t = setTimeout(add, 1000);
    }

    function updateLastRound(){
        timer2.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
        clearTimer();
    }

    function clearTimer(){
        timer1.textContent = "00:00:00";
        seconds = 0; minutes = 0; hours = 0;
    }

    // Rounds
    function addRounds(){
        round++;
        rounds.textContent = round;
    }

    function updateLastGame(){
        lastGame.textContent = round;
        clearRounds();
    }

    function clearRounds() {
        round = 0;
        rounds.textContent = round;
    }

    //user inputs

    function playerinput(){
        clickcount++;
        var id = this.id;
        var val;

        switch(id){
            case "a":
                val = 4;
                break;

            case "b":
                val = 1;
                break;

            case "c":
                val = 3;
                break;

            case "d":
                val = 2;
                break;

            default:
                val = 0;
                break;
        }

        showLight(val);
        setTimeout(function(){
            noLight();
        }, 300);

        userorder.push(val);
        //console.log(userorder);

        if (clickcount === round){
            checkEqual();
        }
    }

    function checkEqual(){

        if(arraysEqual(userorder,generatedOrder)){
            clearUserArray();
            addRounds();
            clickcount = 0;
            startRound();
        }else{
            stopGame();
        }
    }

    function arraysEqual(arr1, arr2) {
        if(arr1.length !== arr2.length)
            return false;
        for(var i = arr1.length; i--;) {
            if(arr1[i] !== arr2[i])
                return false;
        }

        return true;
    }

})(jQuery);