(function($) {

// Rounds
    var round = 0;
    var rounds = document.getElementById('round'),
        lastGame = document.getElementById('lastgame');

    function addRounds() {
        round++;
        rounds.textContent = round;
    }

    function updateLastGame() {
        lastGame.textContent = round;
        clearRounds();
    }

    function clearRounds() {
        round = 0;
        rounds.textContent = round;
    }

})(jQuery);