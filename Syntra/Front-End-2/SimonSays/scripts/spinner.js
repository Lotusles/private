(function($) {

    // Spinner Functions

    function startSpin() {
        return rotate = setInterval(Rotate, 500);
    }

    function stopRotate() {
        clearInterval(rotate);
        image.src = images[0];
        return currentPos = 0;
    }

    function Rotate() {
        if (++currentPos >= images.length) {
            currentPos = 0;
        }
        image.src = images[currentPos];
    }

    function showLight(number) {
        image.src = images[number];
    }

    function noLight() {
        image.src = images[0];
    }

})(jQuery);