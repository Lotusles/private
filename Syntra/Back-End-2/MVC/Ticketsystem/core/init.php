<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 19:16
 */

session_start();

$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => '127.0.0.1',
        'username'=> 'lesley',
        'password'=> 'lesley',
        'db'=> 'ticketsysteem'
    )
);

spl_autoload_register(function($class){
    require_once 'models/'. $class .'.php';
});

require_once "controllers/login.php";
require_once "controllers/user.php";
require_once "controllers/ticket.php";