<?php

include_once "core/init.php";

$loginController = new loginController();

/**
 * Dispatching the world
 */

if(isset($_GET['logout'])){
    session_destroy();
    session_start();
}

if(isset($_GET['register']))
{
    $loginController->showRegister();
    die();
}

if(isset($_POST['submit'])){
    $loginController->loginCheck();
}

$loginController->showLogIn();
