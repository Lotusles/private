<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 26/11/2017
 * Time: 21:30
 */

include_once "core/init.php";
Functions::CheckSessionLogin();

$ticketController = new ticketController();

if(isset($_GET['add']))
{
    $ticketController->showAddTicket();
}
elseif(isset($_GET['edit']))
{
    $ticketController->showEditTicket();
}
else if(isset($_GET['detail']))
{
    $ticketController->showTicketDetails();
}
else
{
    $ticketController->showDashboard();
}

