<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 26/11/2017
 * Time: 21:43
 */

include_once "core/init.php";
Functions::CheckSessionLogin();

$userController = new userController();

if(isset($_GET['add']))
{
    $userController->showAddUser();
}
elseif(isset($_GET['edit']))
{
    $userController->showEditUser();
}
else if(isset($_GET['profile']))
{
    $userController->showUserFrofile();
}
else
{
    $userController->showDashboard();
}