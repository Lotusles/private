<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 22:20
 */

?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
          <title>Document</title>
        <link rel="stylesheet" href="styles/foundation.min.css">
        <link rel="stylesheet" href="styles/app.css">
    </head>
    <body class="grid-container">

        <div class="grid-x">

            <header class="cell">
                <h1 class="text-center">Ticket Support</h1>
            </header>

