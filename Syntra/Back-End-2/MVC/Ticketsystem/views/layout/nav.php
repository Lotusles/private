<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 22:41
 */


$page = isset($GLOBALS["page"]) ? $GLOBALS["page"] : '';

echo "<nav class='small-12 medium-3 large-2 cell'>",
    "<h3>Menu</h3>",
    "<ul class='vertical menu drilldown' data-drilldown>";

if($page === "login" || $page === "register"){
    echo "<li><a href='index.php'>Login</a>",
    "<li><a href='index.php?register'> Register</a>";
}else{
    echo "<li><a href='#'>Tickets</a>",
            "<ul class=\"menu vertical nested\">",
                "<li><a href=\"tickets.php\">Bekijk Tickets</a></li>",
                "<li><a href=\"tickets.php?add\">Voeg Ticket toe</a></li>",
            "</ul>",
        "</li>",
        "<li><a href='#'>Users</a>",
            "<ul class=\"menu vertical nested\">",
                "<li><a href=\"users.php\">Bekijk Users</a></li>",
                "<li><a href=\"users.php?add\">Voeg User toe</a></li>",
            "</ul>",
        "</li>",
        "<li><a href='index.php?logout'>Logout</a></li>";
}

echo " </ul>",
    "</nav>";

    echo "<main class='small-12 medium-auto cell'>";
?>