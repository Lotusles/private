<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 21:27
 */
$HTML = new HTML();

$input_usrname = isset($_POST['usrname']) ? $_POST['usrname'] : "" ;

echo
    "<form action='index.php' method='post' data-abide novalidate> \n",
    "<fieldset class='fieldset'> \n",
    "<legend>Login</legend> \n";

    $HTML->FormInputGroup("Username", "usrname", "text", $input_usrname, true);
    $HTML->FormInputGroup("Password", "pasword", "password", "", true );

echo    "<div data-abide-error class=\"alert callout\" style=\"display: none;\">
            <p><i class=\"fi-alert\"></i> There are some errors in your form.</p>
        </div>",
        "<div class='text-right'> \n ",
            "<input type='submit' name='submit' class='button expanded' value='Log In'> \n",
        "</div> \n",
    "</fieldset> \n",
    "</form> \n";