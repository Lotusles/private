<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 21:27
 */

    $HTML = new HTML();

    $FName = (isset($_POST["fname"])) ? $_POST["fname"] : "";
    $LName = (isset($_POST["lname"])) ? $_POST["lname"] : "";
    $Email = (isset($_POST["email"])) ? $_POST["email"] : "";
    $Username = (isset($_POST["usrname"])) ? $_POST["usrname"] : "";

    echo
        "<form action='".$_SERVER['PHP_SELF']."' method='post' data-abide novalidate> \n",
            "<fieldset class='fieldset'> \n",
                "<legend>Register</legend> \n";

                $HTML->FormInputGroup("Firstname", "fname", "text", $FName, true);
                $HTML->FormInputGroup("Lastname", "lname", "text", $LName, true);
                $HTML->FormInputGroup("E-mail", "email" ,"email", $Email, true);
                $HTML->FormInputGroup("Username", "usrname", "text", $Username, true);
                $HTML->FormInputGroup("Password", "pasword", "password","", true);
    echo
                "<div data-abide-error class=\"alert callout\" style=\"display: none;\">
                    <p><i class=\"fi-alert\"></i> There are some errors in your form.</p>
                </div>",
                "<div class='text-right'>",
                    "<input type='submit' name='register' class='button expanded' value='Register'> \n",
                "</div>",
            "</fieldset> \n",
        "</form> \n";