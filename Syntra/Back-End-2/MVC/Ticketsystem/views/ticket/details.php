<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 26/11/2017
 * Time: 22:14
 */

?>

<div class="grid-x">
    <div class="small-12 cell">
        <h4>Ticket from: <?php echo $detail["UserId"] ?></h4>
        <p><a href="?edit&id=<?php echo $detail["Id"] ?>">Edit Ticket</a></p>
        <br>
        <p>Priority: <?php echo $detail["Prior"] ?></p>
        <p>Status: <?php echo $detail["Status"] ?></p>
        <p>Onderwerp: <?php echo $detail["Subject"] ?></p>
        <p>Bericht:<br> <?php echo $detail["Message"] ?></p>
    </div>
</div>
