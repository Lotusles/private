<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 26/11/2017
 * Time: 22:14
 */

$HTML = new HTML();

$userid = "1";
$subject = (isset($_POST["subject"])) ? $_POST["subject"] : "";
$status = "1";
$pior = "1";
$message = (isset($_POST["message"])) ? $_POST["message"] : "";

echo
    "<form action='".$_SERVER['PHP_SELF']."' method='post' data-abide novalidate> \n",
"<fieldset class='fieldset'> \n",
"<legend>Add Ticket</legend> \n";

$HTML->FormInputGroup("Subject", "subject", "text", $subject, true);
$HTML->FormInputGroup("Message", "message", "text", $message, true);

echo
"<div data-abide-error class=\"alert callout\" style=\"display: none;\">
                        <p><i class=\"fi-alert\"></i> There are some errors in your form.</p>
                    </div>",
"<div class='text-right'>",
"<input type='submit' name='ticket' class='button expanded' value='Submit Ticket'> \n",
"</div>",
"</fieldset> \n",
"</form> \n";