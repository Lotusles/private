<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 27/11/2017
 * Time: 22:55
 */


$HTML = new HTML();

echo
    "<form action='".$_SERVER['PHP_SELF']."?detail&id=".$detail['Id']."' method='post' data-abide novalidate> \n",
"<fieldset class='fieldset'> \n",
"<legend>Edit Ticket</legend> \n";

$HTML->FormInputGroup("Id", "id", "text", $detail["Id"], true);
$HTML->FormInputGroup("Subject", "subject", "text", $detail["Subject"], true);
$HTML->FormInputGroup("Message", "message", "text", $detail["Message"], true);

echo
"<div data-abide-error class=\"alert callout\" style=\"display: none;\">
                        <p><i class=\"fi-alert\"></i> There are some errors in your form.</p>
                    </div>",
"<div class='text-right'>",
"<input type='submit' name='ticket_edit' class='button expanded' value='Edit Ticket'> \n",
"</div>",
"</fieldset> \n",
"</form> \n";