<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 21:27
 */

$HTML = new HTML();


echo
    "<form action='".$_SERVER['PHP_SELF']."?profile&id=".$user['Id']."' method='post' data-abide novalidate> \n",
"<fieldset class='fieldset'> \n",
"<legend>Edit Profile</legend> \n";

$HTML->FormInputGroup("Id", "id", "text", $user["Id"], true);
$HTML->FormInputGroup("Firstname", "fname", "text", $user["FName"], true);
$HTML->FormInputGroup("Lastname", "lname", "text", $user["LName"], true);
$HTML->FormInputGroup("E-mail", "email" ,"email", $user["Email"], true);
$HTML->FormInputGroup("Username", "usrname", "text", $user["Username"], true);

echo
"<div data-abide-error class=\"alert callout\" style=\"display: none;\">
                    <p><i class=\"fi-alert\"></i> There are some errors in your form.</p>
                </div>",
"<div class='text-right'>",
"<input type='submit' name='profile_edit' class='button expanded' value='Edit Profile'> \n",
"</div>",
"</fieldset> \n",
"</form> \n";