<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 26/11/2017
 * Time: 22:15
 */
?>

<div class="grid-x">
    <div class="small-12 cell">
        <h4>Profile: <?php echo $user["Username"] ?></h4>
        <p><a href="?edit&id=<?php echo $user["Id"] ?>">Edit Profile</a></p>
        <br>
        <p>Firstname: <?php echo $user["FName"] ?></p>
        <p>Lastname: <?php echo $user["LName"] ?></p>
        <p>E-mail: <?php echo $user["Email"] ?></p>
    </div>
</div>
