<?php

class userController {

    public function showDashboard() {
        $counter = 0;
        $users = new User;
        $Allusers = $users->getAllUsers();
        $table = new Table;
        $row = new Row;

        $id = "Id";

        if(isset($_POST["register"]) && $_POST["register"] != ""){

            if(!$users->addUser())
            {
                echo "Something went wrong!";
                die();
            }

            $users->CloseConn();
        }

        $row->append(new Cell('<strong>#</strong>'));
        $row->append(new Cell('<strong>Firstname</strong>'));
        $row->append(new Cell('<strong>Lastname</strong>'));
        $row->append(new Cell('<strong>E-mail</strong>'));
        $row->append(new Cell('<strong>Details</strong>'));
        $table->append($row);

        foreach ($Allusers as $Table => $Row){
            $row = new Row();

            foreach ($Allusers[$counter] as $key=>$value) {
                $row->append(new Cell($value));
            }

            $indentify = $Allusers[$counter][$id];

            $row->append(new Cell("<a href='?profile&id=$indentify'>Details</a>"));

            $counter++;

            $table->append($row);
        }

        Layout::PageHeader();
        Layout::PageNav();



        include_once "views/users/index.php";
        Layout::PageFooter();
    }

    public function showEditUser() {
        $users = new User;
        $user = $users->getUserById($_GET["id"]);

        Layout::PageHeader();
        Layout::PageNav();
        include_once "views/users/edit.php";
        Layout::PageFooter();
    }

    public function showUserFrofile() {
        $users = new User;

        if(isset($_POST["profile_edit"])){

            if(!$users->updateUser())
            {
                echo "Something went wrong!";
                die();
            }
            $users->CloseConn();
            $users = [];
            $users = new User;
        }

        $user = $users->getUserById($_GET["id"]);

        Layout::PageHeader();
        Layout::PageNav();
        include_once "views/users/profile.php";
        Layout::PageFooter();
    }
}