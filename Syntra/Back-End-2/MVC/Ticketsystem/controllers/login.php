<?php

class loginController {

    public function showLogIn() {
        $GLOBALS["page"] = "login";
        Layout::PageHeader();
        Layout::PageNav();

        if(isset($_POST["register"]) && $_POST["register"] != ""){
            $users = new User;

            if(!$users->addUser())
            {
                echo "Something went wrong!";
                die();
            }

            $users->CloseConn();
        }

        include_once "views/login/index.php";
        Layout::PageFooter();
    }

    public function showRegister() {
        $GLOBALS["page"] = "register";
        Layout::PageHeader();
        Layout::PageNav();
        include_once "views/login/register.php";
        Layout::PageFooter();
    }

    public function loginCheck(){

        $users = new User;

        $input_usrname = isset($_POST['usrname']) ? $_POST['usrname'] : " " ;
        $input_passworde = isset($_POST['pasword']) ? $_POST['pasword'] : " " ;

        $user =$users->getUserByName();

        if($user["Username"] === $input_usrname && $user["Password"] === $input_passworde){
            $_SESSION['login'] = true;
            Functions::RelocatePHP('tickets.php');
        }else{
            $this->showLogIn();
        }
    }
}