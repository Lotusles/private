<?php

class ticketController {

    public function showDashboard() {

        $counter = 0;

        $tickets = new Ticket();
        $Alltickets = $tickets->getAllTickets();

        $table = new Table;
        $row = new Row;

        $id = "Id";

        if(isset($_POST["ticket"])){

            if(!$tickets->addTicket())
            {
                echo "Something went wrong!";
                die();
            }

            $tickets->CloseConn();
        }

        $row->append(new Cell('<strong>#</strong>'));
        $row->append(new Cell('<strong>Onderwerp</strong>'));
        $row->append(new Cell('<strong>Priorty</strong>'));
        $row->append(new Cell('<strong>Details</strong>'));
        $table->append($row);

        foreach ($Alltickets as $Table => $Row){
            $row = new Row();
            foreach ($Alltickets[$counter] as $key=>$value) {
                $row->append(new Cell($value));
            }

            $indentify = $Alltickets[$counter][$id];

            $row->append(new Cell("<a href='?detail&id=$indentify'>Details</a>"));

            $counter++;

            $table->append($row);
        }

        Layout::PageHeader();
        Layout::PageNav();



        include_once "views/ticket/index.php";
        Layout::PageFooter();
    }

    public function showAddTicket() {
        Layout::PageHeader();
        Layout::PageNav();
        include_once "views/ticket/edit.php";
        Layout::PageFooter();
    }

    public function showEditTicket() {
        $tickets = new Ticket();
        $detail = $tickets->getTicket($_GET["id"]);

        Layout::PageHeader();
        Layout::PageNav();
        include_once "views/ticket/edit.php";
        Layout::PageFooter();
    }

    public function showTicketDetails() {

        $tickets = new Ticket();

        if(isset($_POST["ticket_edit"])){

            if(!$tickets->updateTicket())
            {
                echo "Something went wrong!";
                die();
            }
            $tickets->CloseConn();
            $tickets = [];
            $tickets = new Ticket();
        }

        $detail = $tickets->getTicket($_GET["id"]);

        Layout::PageHeader();
        Layout::PageNav();
        include_once "views/ticket/details.php";
        Layout::PageFooter();
    }
}