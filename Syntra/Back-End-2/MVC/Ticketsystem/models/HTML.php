<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 20:55
 */

class HTML
{
    /**
     * @param string $header_title
     */
    public function H1(string $header_title){
        echo "<h1>$header_title</h1> \n";
    }

    public function H2(string $header_title){
        echo "<h2>$header_title</h2> \n";
    }

    public function H3(string $header_title){
        echo "<h3>$header_title</h3> \n";
    }

    public function H4(string $header_title){
        echo "<h4>$header_title</h4> \n";
    }
    public function H5(string $header_title){
        echo "<h5>$header_title</h5> \n";
    }

    public function H6(string $header_title){
        echo "<h6>$header_title</h6> \n";
    }

    public function Paragraph($string){
        echo "<p>$string</p> \n";
    }

    public function FormInputGroup($label, $id, $type = "text", $value = "", $required = false){

        echo
            "<div class='input_group'>\n",
                "<label for='$id'>".$label.":</label> \n",

                "<input id='$id' type='$type' name='$id' value='$value'";
                if($required){
                    echo " required ";
                }
        echo    "> <span class=\"form-error\">Yo, you had better fill this out.</span>\n",
            "</div>\n"
        ;
    }
}