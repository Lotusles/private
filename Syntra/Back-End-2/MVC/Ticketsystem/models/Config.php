<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 19:16
 */

class Config
{
    public static function get($path = null){
        if ($path){

            $config = $GLOBALS['config'];
            $path = explode('/' ,$path);

            foreach ($path as $bit){
                if (isset($config[$bit])){
                    $config = $config[$bit];
                }
            }

            return $config;
        }

        return FALSE;
    }
}