<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 29/10/2017
 * Time: 09:00
 */



class Cell {
    private $_content;

    public function __construct($content) {
        $this->_content = $content;
    }

    public function getContent() {
        return $this->_content;
    }
}
