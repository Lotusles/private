<?php

class DB {
    private $conn;
    private $error;
    private $results;
    private $lastId;
    private $stmt;
    private $rowCount;

    public function __construct(){
        try {

            $this->conn = new PDO('mysql:host=' .Config::get('mysql/host').  ';dbname=' . Config::get('mysql/db'),Config::get('mysql/username'),Config::get('mysql/password'));
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return true;
        }
        catch(PDOException $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError(string $error)
    {
        $this->error = $error;
    }

    /**
     * @return mixed
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param mixed
     */
    public function getResult()
    {
        return $this->results[0];
    }

    /**
     * @return mixed
     */
    public function getLastId()
    {
        return $this->lastId;
    }

    /**
     * @param mixed $lastId
     */
    public function setLastId($lastId)
    {
        $this->lastId = $lastId;
    }

    public function CloseConn()
    {
        $this->conn = null;
    }

    public function ClearStmt(){

        $this->stmt = null;
    }

    public function Execute($sql)
    {
        try {

            $this->stmt = $this->conn->prepare($sql);
            $this->stmt->execute();
            $this->ClearStmt();

            return true;
        }
        catch(PDOException $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
    }

    public function CreateDB($DBName)
    {
        try {

            $sql = "CREATE DATABASE ".$DBName;

            // Prepare statement
            $this->stmt = $this->conn->prepare($sql);

            // execute the query
            $this->stmt->execute();
            $this->ClearStmt();

            return true;
        }
        catch(PDOException $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
    }

    public function CreateTable($sql)
    {
        try {

            $this->stmt = $this->conn->prepare($sql);
            $this->stmt->execute();
            $this->ClearStmt();

            return true;
        }
        catch(PDOException $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
    }

    public function InsertData($sql)
    {
        try {

            $this->stmt = $this->conn->prepare($sql);
            $this->stmt->execute();
            $this->lastId = $this->conn->lastInsertId();
            $this->ClearStmt();

            return true;
        }
        catch(PDOException $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
    }

    public function InsertMultiData($SQLarray)
    {
        try
        {
            $this->conn->beginTransaction();

            foreach ($SQLarray as $key => $value)
            {
                $this->conn->exec($value);
            }

            $this->conn->commit();
            $this->lastId = $this->conn->lastInsertId();
            $this->ClearStmt();

            return true;
        }
        catch(PDOException $e)
        {
            $this->conn->rollback();
            $this->setError($e->getMessage());

            return false;
        }
    }

    public function SelectData($sql)
    {
        try
        {
            $this->stmt = $this->conn->prepare($sql);
            $this->results = array();
            ;
            if ($this->stmt->execute()) {
                while ($row = $this->stmt->fetch(PDO::FETCH_ASSOC)) {
                    $this->results[] = $row;
                }
            }

            /*$this->stmt->setFetchMode(PDO::FETCH_ASSOC);
            $this->results = $this->stmt->fetchAll();*/

            return true;
        }
        catch(PDOException $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
    }

    public function DeleteData($sql)
    {
        try
        {
            $this->stmt = $this->conn->prepare($sql);
            $this->stmt->execute();
            $this->rowCount = $this->stmt->rowCount();
            $this->ClearStmt();

            return true;
        }
        catch(PDOException $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
    }

    public function UpdateData($sql)
    {
        try
        {
            $this->stmt = $this->conn->prepare($sql);
            $this->stmt->execute();
            $this->rowCount = $this->stmt->rowCount();
            $this->ClearStmt();

            return true;
        }
        catch(PDOException $e)
        {
            $this->setError($e->getMessage());

            return false;
        }
    }
}