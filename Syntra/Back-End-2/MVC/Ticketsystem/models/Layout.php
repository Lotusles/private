<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 23:04
 */

class Layout
{

    public static function PageHeader(){
        include_once ("views/layout/header.php");
    }

    public static function PageNav(){
        include_once ("views/layout/nav.php");
    }

    public static function PageFooter(){
        include_once ("views/layout/footer.php");
    }
}