<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 25/11/2017
 * Time: 21:40
 */

class Functions{

    /**
     * @string $URL
     */
    public static function RelocatePHP($URL){
        ob_start(); // ensures anything dumped out will be caught

        // clear out the output buffer
        while (ob_get_status())
        {
            ob_end_clean();
        }
        // no redirect
        header( "Location: $URL" );
    }

    public static function currentURL() {
        return $_SERVER["REQUEST_URI"];
    }

    public static function CheckSessionLogin(){

        $admin = false;

        if(isset($_SESSION['login'])){
            $admin = $_SESSION['login'];
        }

        if(!$admin){
            Functions::RelocatePHP("index.php");
        }
    }

}