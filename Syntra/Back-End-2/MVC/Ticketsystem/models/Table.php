<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 29/10/2017
 * Time: 08:57
 */


class Table
{
    private $_rows;

    public function __construct() {
        $this->_rows = array();
    }

    public function append($row) {
        $this->_rows[] = $row;
    }

    public function draw()
    {
        echo '<table>' . PHP_EOL; // Begin van de tabel, border voor de duidelijkheid

        foreach ($this->_rows as $row) {

            echo '<tr>' . PHP_EOL;

            foreach ($row->getCells() as $cell) {
                echo '<td>' . $cell->getContent() . '</td>' . PHP_EOL;
            }

            echo '</tr>' . PHP_EOL;
        }
        echo '</table>' . PHP_EOL;
    }
}