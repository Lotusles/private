<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 05/12/2017
 * Time: 19:57
 */

namespace app\models;

use Yii;

use yii\base\Model;

class Contact extends \yii\db\ActiveRecord{

    /**

     * @inheritdoc

     */

    public static function tableName()

    {

        return 'contact';

    }

    /**

     * @inheritdoc

     */

    public function rules()

    {

        return [

            [['name','email','message'], 'required'],

            ['email', 'email'],

            [['name'],'string', 'max' => 50],

            [['email'], 'string', 'max' => 50],

            [['message'], 'string', 'max' => 255],



        ];

    }

}