<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 12:14
 */

require_once 'core/init.php';
require_once 'includes/header.php';

if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["submit"]) ) {

    $date = $_POST['days'] . '/'. $_POST['months'] . '/'. $_POST['year'];

    if ( isset($_FILES["file"])) {

        //if there was an error uploading the file
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        }
        else {
            //Print file details
            echo "Uploads: " . $_FILES["file"]["name"] . "<br />";
            echo "Type: " . $_FILES["file"]["type"] . "<br />";
            echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
            echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";

            //if file already exists
            if (file_exists("uploads/" . $_FILES["file"]["name"])) {
                echo $_FILES["file"]["name"] . " already exists. ";
            }
            else {
                //Store file in directory "uploads" with the name of "uploaded_file.txt"

                $storageName = "cursisten.txt";

                    move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/" . $storageName);
                echo "Stored in: " . "uploads/" . $_FILES["file"]["name"] . "<br /><br />";


                $_count = 0;
                $storagename = $_SESSION["storageName"] = "uploads/" . $storageName;
                $cursisten = array();

                if ( $handle = fopen( $storagename , "r" ) ) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        if($_count++>0) {
                            array_push($cursisten, new Cursist($data[0], $data[1]));
                        }
                    }
                }

                $obj = new Opleiding($_POST['education'], $cursisten, $date);
                $_SESSION['education'] = serialize($obj);

                echo "<a href=\"readfile.php\">Bekijk de CSV</a>";
            }
        }
    } else {
        echo "No file selected <br />";
    }
}

require_once 'includes/footer.php';