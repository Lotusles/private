<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 12:06
 */

function escape($string){
    return htmlentities($string, ENT_QUOTES,'UTF-8');
}