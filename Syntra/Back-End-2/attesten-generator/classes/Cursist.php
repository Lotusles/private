<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 12:48
 */

class Cursist
{
    private $firstName;
    private $lastName;

    /**
     * Cursist constructor.
     * @param $firstname
     * @param $lastname
     */
    function __construct($firstname, $lastname)
    {

        $this->firstName = $firstname;
        $this->lastName = $lastname;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }
}