<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 22:01
 */

class createPDF
{
    private $opleiding;
    private $date;
    private $firstname;
    private $lastname;
    private $path;

    private $pdf;

    function __construct($opleiding, $firstname, $lastname, $date)
    {
        $this->opleiding = $opleiding;
        $this->date = $date;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }

    public function create_PDF(){

        $this->pdf = new PDFE();

        $this->pdf->SetTitle( ''.$this->opleiding);
        $this->pdf->SetAuthor('Syntra');
        $this->pdf->PrintChapter($this->opleiding, $this->firstname ,$this->lastname, $this->date);

        $this->path = 'pdf/'.$this->firstname. $this->lastname.'.pdf';
        $content = $this->pdf->Output('pdf/'.$this->firstname. $this->lastname.'.pdf','F');
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    public function show_PDF(){
        return $this->pdf->Output();
    }

    public function download_PDF(){

        $content = $this->pdf->Output('pdf/'.$this->firstname. $this->lastname.'.pdf','F');

        $url_download = $this->path;

//header("Content-type:application/pdf");
        header("Content-type: application/octet-stream");
        header("Content-Disposition:inline;filename='".basename($this->path)."'");
        header('Content-Length: ' . filesize($this->path));
        header("Cache-control: private"); //use this to open files directly
        readfile($this->path);
    }

}