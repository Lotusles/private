<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 12:43
 */

class Opleiding
{
    private $education;
    private $date;
    private $cursisten;


    /**
     * Opleiding constructor.
     * @param $edu
     * @param $cursisten
     */
    function __construct($edu, $cursisten, $date)
    {
        $this->education = $edu;
        $this->date = $date;
        $this->cursisten = $cursisten;

    }

    /**
     * @return mixed
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * @return mixed
     */
    public function getCursisten()
    {
        return $this->cursisten;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }
}

