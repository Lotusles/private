<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 21:28
 */

require_once 'core/init.php';
require_once 'includes/header.php';

$opleiding = unserialize($_SESSION["education"]);

$cursisten = $opleiding->getCursisten();

$zip = new ZipArchive();
$zip_name = time().".zip"; // Zip name

$zip->open($zip_name,  ZipArchive::CREATE);

$files = [];

foreach($_POST as $key => $value){
    $id = $key - 1;
    $pdf = new createPDF( $opleiding->getEducation(), $cursisten[$id]->getFirstName(), $cursisten[$id]->getLastName(), $opleiding->getDate());
    $pdf->create_PDF();

    $files[] = $pdf->getPath();
}

foreach ($files as $path) {

    if(file_exists($path)){
        $zip->addFromString(basename($path),  file_get_contents($path));
    }
    else{
        echo"file does not exist";
    }
}
$zip->close();

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"".$zip_name."\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($zip_name));
ob_end_clean();
flush();
readfile("$zip_name");

require_once 'includes/footer.php';