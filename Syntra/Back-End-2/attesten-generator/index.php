<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 12:06
 */

require_once 'core/init.php';

require_once 'includes/header.php';

?>


<main>
    <h1>Attesten Generator</h1>

    <form action="upload.php" method="post" enctype="multipart/form-data">
        <label>
            Select CSV to upload: <br/>
            <input type="file" name="file" id="file" required/>
        </label>
        <br/>
        <br/>
        <label>
            Opleiding: <br/>
            <input type="text" name="education" placeholder="Webontwikkelaar" required />
        </label>
        <br/>
        <br/>
        <label>
            Datum van Slagen: <br/>
            <select id="daydropdown" name="days">
            </select>
            <select id="monthdropdown" name="months">
            </select>
            <select id="yeardropdown" name="year">
            </select>
        </label>
        <br/><br/>
        <input type="submit" value="Upload CSV" name="submit"/>
    </form>
</main>


<?php
require_once 'includes/footer.php';
?>