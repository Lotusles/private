<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 12:05
 */


session_start();

/**
 * Standard PHP Library:
 * Autoload function for classes
 */
spl_autoload_register(function($class){
    require_once 'classes/'. $class .'.php';
});

require_once 'functions/sanitize.php';
require_once 'fpdf/fpdf.php';