<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 12:17
 */

require_once 'core/init.php';
require_once 'includes/header.php';

$opleiding = unserialize($_SESSION["education"]);
$cursisten = $opleiding->getCursisten();

echo '<h1>Opleiding: '.$opleiding->getEducation().'</h1>';
echo '<p>Datum van slagen: '. $opleiding->getDate() .'</p>';

$table = new Table();
$row1 = new Row();

$rowCount = 1;

$row1->append(new Cell(' '));
$row1->append(new Cell('FirstName'));
$row1->append(new Cell('LastName'));
$row1->append(new Cell('Download'));
$row1->append(new Cell('Preview'));

$table->append($row1);


foreach ($cursisten as $cursist){

    ${"row$rowCount"} = new Row();

    ${"row$rowCount"}->append(new Cell('<input type="checkbox" id="checkbox_'. $rowCount .'" name="'. $rowCount .'">'));
    ${"row$rowCount"}->append(new Cell('<label for="checkbox_'. $rowCount .'">'.$cursist->getFirstName().'<label>'));
    ${"row$rowCount"}->append(new Cell('<label for="checkbox_'. $rowCount .'">'.$cursist->getLastName().'<label>'));
    ${"row$rowCount"}->append(new Cell('<a href="handler.php?method=download&id='. ($rowCount - 1) .'">Download</a>'));
    ${"row$rowCount"}->append(new Cell('<a href="handler.php?method=view&id='. ($rowCount - 1) .'">Preview</a>'));

    //echo $cursist->getFirstName().' '. $cursist->getLastName() . '<br>';

    $table->append(${"row$rowCount"});

    $rowCount++;
}

echo '<form method="POST" action="download.php">';

$table->draw();

echo '<br><input type="submit" value="Download Selected">';
echo '</form>';

require_once 'includes/footer.php';