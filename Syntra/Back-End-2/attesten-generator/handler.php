<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 04/11/2017
 * Time: 18:58
 */

require_once 'core/init.php';

$opleiding = unserialize($_SESSION["education"]);
$cursisten = $opleiding->getCursisten();

$id = $_GET["id"];

$pdf = new createPDF($opleiding->getEducation(), $cursisten[$id]->getFirstName(),$cursisten[$id]->getLastName(),$opleiding->getDate());

$pdf->create_PDF();

$download = !empty($_GET["method"]) && $_GET['method'] == 'download' ? True : False;

if($download == TRUE){
    $pdf->download_PDF();
}

$pdf->show_PDF();

unset($pdf);