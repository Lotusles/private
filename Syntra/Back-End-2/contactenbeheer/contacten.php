<?php
// Start the session
session_start();

//Controleer if logged in
if(!isset($_SESSION['logged_in'])) {
    header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>



<div class="container">
    <div class="jumbotron">
        <h1>Contacten beheren</h1>
        <div class="row">
            <div>
                <div class="col-md-6 col-md-offset-3">
                    <?php //Zoekveld contacten ?>
                    <input type="text">
                </div>
                <div class="col-md-3">
                    <?php //toevoegen contact ?>
                    <a href="new.php">Add</a>
                </div>
            </div>
        </div>
        <?php //De lijst ?>

        <?php

        //DB connectie
        $db = new mysqli('localhost', 'syntra', 'qbcdef', 'syntra');
        // Check connection
        if ($db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }

        //Query
        $sql = "SELECT * FROM contacten";
        $result = $db->query($sql);

        if ($result->num_rows > 0) {
            //Loop
            while($row = $result->fetch_assoc()) {
                ?>

                <div class="row">
                    <div class="col-md-6">
                        <?php echo $row['name']; ?>
                    </div>
                    <div class="col-md-6">
                        <?php echo $row['email']; ?>
                    </div>
                </div>
                <?php
            }
        } else {
            echo "0 results";
        }
        $db->close();

        ?>

    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="custom.js"></script>
</body>
</html>