<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 02/11/2017
 * Time: 21:49
 *
 * CLASS : Config
 *
 * Makes it possible to pull a config file from any where
 * checks if settings file is set.
 * Used to store our DB settings.
 *
 *
 * Examples:
 *
 *  echo Config::get('mysql/host'); // '127.0.0.1';
 *
 */

class Config
{
    public static function get($path = null){
        if ($path){

            $config = $GLOBALS['config'];
            $path = explode('/' ,$path);

            foreach ($path as $bit){
                if (isset($config[$bit])){
                    $config = $config[$bit];
                }
            }

            return $config;
        }

        return FALSE;
    }
}