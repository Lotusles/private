<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 02/11/2017
 * Time: 21:50
 *
 * CLASS : DB
 * Makes it possible to execute Query`s into the DB by a helper class
 *
 *
 * Example Getting Results:
 *  $user = DB::getInstance()->get('users', array('username', '=', 'alex'));
 *
 *  // Loops true the database to return the first username of many
 *  if (!$user->count()){
 *      echo 'No user';
 *  }else{
 *      echo $user->first()->username;
 *  }
 *
 *
 * Example Insert/Update Record:
 *  $userInsert = DB::getInstance()->insert('users', array(
 *      'username' => 'Dale',
 *      'password' => 'password',
 *      'salt' => 'salt',
 *      'name' => 'Dale Dogs',
 *      'email' =>  'DaleDogs@Hotmail.com',
 *      'group' => '1'
 *  ));
 *
 *  if ($userInsert){
 *      echo "Success! ";
 *  }else{
 *      echo "Failed";
 *  }
 *
 *
 * Example Deleting Records:
 */

class DB{
    private static $_instance = null;
    private $_pdo,
        $_query,
        $_error = false,
        $_results,
        $_count = 0;

    private function __construct(){
        try{
            $this -> _pdo = new PDO('mysql:host=' .Config::get('mysql/host').  ';dbname=' . Config::get('mysql/db'),Config::get('mysql/username'),Config::get('mysql/password'));
            //echo 'Connected';
        }
        catch (PDOException $e){
            die($e->getMessage());
        }
    }

    public static function getInstance(){
        if(!isset(self::$_instance)){
            self::$_instance=new DB();
        }
        return self::$_instance;
    }

    public function query($sql, $params = array()){
        $this->_error = false;
        if($this->_query = $this->_pdo->prepare($sql)){
            $x=1;
            if(count($params)){
                foreach($params as $param){
                    $this->_query->bindValue($x, $param);
                    $x++;
                }
            }
            if($this->_query->execute()){
                $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
                $this->_count = $this->_query->rowCount();
 				//echo 'Success';
 			}
            else{
                $this->_error = true;
                //echo 'Not Success';
            }
        }
        return $this;
    }

    public function action($action, $table, $where=array()){
        if(count($where)===3){
            $operators = array('=','>','<','>=','<=');

            $field =$where[0];
            $operator = $where[1];
            $value = $where[2];

            if (in_array($operator,$operators)){
                $sql="{$action} FROM {$table} WHERE {$field} {$operator} ?";

                if(!$this->query($sql, array($value))->error()){
                    return $this;
                }
            }
        }
        return false;
    }

    public function insert($table, $fields = array()){
        if(count($fields)){
            $keys = array_keys($fields);
            $values = "";
            $x = 1;

            foreach($fields as $field)	{
                $values .='?';
                if($x<count($fields)){
                    $values .=', ';
                }
                $x++;

            }
            //die($values);

            $sql="INSERT INTO users (`".implode('`,`',$keys)."`) VALUES ({$values})";
            //$sql = "INSERT INTO `{$table}` (`".implode('`, `', $keys)."`) VALUES (".str_repeat ( "?, " , count($keys)-1 )."?)";

            echo $sql;

            if(!$this->query($sql,$fields)->error()){
                return true;
            }

            return false;
        }
    }

    public function update($table, $id, $fields){
        $set='';
        $x=1;

        foreach($fields as $name => $value){
            $set .= "{$name} =?";
            if($x<count($fields)){
                $set .= ', ';
            }
            $x++;
        }
        //die($set);

        $sql="UPDATE {$table} SET {$set} WHERE id = {$id}";
        //echo $sql;

        if(!$this->query($sql,$fields)->error()){
            return true;
        }
        return false;
    }

    public function get($table, $where ){
        return $this->action('SELECT *', $table, $where);
    }

    public function first(){
        return $this->_results[0];
    }

    public function delete($table, $where){
        return $this->action('DELETE *', $table, $where);
    }

    public function results(){
        return $this->_results;
    }

    public function error(){
        return $this->_error;
    }

    public function count(){
        return $this->_count;
    }
}