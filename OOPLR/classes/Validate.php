<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 02/11/2017
 * Time: 21:53
 */

class Validate
{
    private $_passed = False,
        $_error = array(),
        $_db = null;

    /**
     * Validate constructor.
     */
    public function __construct()
    {
        $this->_db = DB::getInstance();
    }

    /**
     * @param $source
     * @param array $items
     * @return $this
     */
    public function check($source, $items = array()){
        foreach ($items as $item => $rules){

            $itemName = $rules['name'];

            foreach ($rules as $rule => $rule_value){

                $value = trim($source[$item]);
                $item = escape($item);

                if ($rule == 'required' && empty($value)){
                    $this->addError("{$item} is required");
                }else if(!empty($value)){
                    switch ($rule){
                        case 'min':
                            if (strlen($value) < $rule_value){
                                $this->addError("{$itemName} must be a minimum of {$rule_value} characters");
                            }
                        break;

                        case 'max':
                            if (strlen($value) > $rule_value){
                                $this->addError("{$itemName} must be a maximum of {$rule_value} characters");
                            }
                        break;

                        case 'matches':
                            if ($value != $source[$rule_value]){
                                $this->addError("{$rule_value} must match {$item}");
                            }
                        break;

                        case 'unique':
                            $check = $this->_db->get($rule_value, array($item, '=', $value));
                            if ($check->count()){
                                $this->addError("{$item} already exist.");
                            }
                        break;

                    }
                }

            }
        }

        if (empty($this->_error)){
            $this->_passed = true;
        }

        return $this;
    }

    /**
     * @param $error
     */
    private function addError($error){
        $this->_error[] = $error;
    }

    /**
     * @return array
     */
    public function errors(){
        return $this->_error;
    }


    public function passed()
    {
        return $this->_passed;
    }
}