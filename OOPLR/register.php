<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 02/11/2017
 * Time: 21:48
 */

require_once 'core/init.php';

if (Input::exists()){

    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'username' => array(
            'name'=> 'Username',
            'required' => true,
            'min' => 2,
            'max' => 20,
            'unique' => 'users'
        ),
        'password' => array(
            'name'=> 'Password',
            'required' => true,
            'min' => 6,
        ),
        'password_again' => array(
            'name'=> 'Password again',
            'required' => true,
            'matches' => 'password'
        ),
        'name' => array(
            'name'=> 'Name',
            'required' => true,
            'min' => 2,
            'max' => 50
        )
    ));

    if ($validation->passed()){
        echo 'Passed';
    }else{
        foreach ($validation->errors() as $error){
            echo $error.'<br>';
        }
    }
}

?>

<form action="" method="post">
    <div class="field">
        <label for="username">Username</label>
        <input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off">
    </div>

    <div class="field">
        <label for="password">Password</label>
        <input type="password" name="password" id="password">
    </div>

    <div class="field">
        <label for="password_again">Enter your password again</label>
        <input type="password" name="password_again" id="password_again">
    </div>

    <div class="field">
        <label for="name">Your name</label>
        <input type="text" name="name" id="name" value="<?php echo escape(Input::get('name')); ?>">
    </div>

    <input type="submit" value="Register">
</form>
