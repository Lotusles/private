<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 02/11/2017
 * Time: 21:54
 */

session_start();

$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => '127.0.0.1',
        'username'=> 'lesley',
        'password'=> 'lesley',
        'db'=> 'ooplr'
    ),
    'remember' => array(
        'cookie_name' => 'hash',
        'cookie_expiry' => 604800
    ),
    'session' => array(
        'session_name' => 'user'
    )
);

/**
 * Standard PHP Library:
 * Autoload function for classes
 */
spl_autoload_register(function($class){
    require_once 'classes/'. $class .'.php';
});

require_once 'functions/sanitize.php';

