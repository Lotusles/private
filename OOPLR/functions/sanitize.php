<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 02/11/2017
 * Time: 21:54
 */

function escape($string){
    return htmlentities($string, ENT_QUOTES,'UTF-8');
}