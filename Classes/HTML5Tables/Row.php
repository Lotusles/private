<?php
/**
 * Created by PhpStorm.
 * User: minis
 * Date: 29/10/2017
 * Time: 08:59
 */

namespace HTML5Tables {

    class Row {
        private $_cells;

        public function __construct() {
            $this->_cells = array();
        }

        public function append($cell) {
            $this->_cells[] = $cell;
        }

        public function getCells() {
            return $this->_cells;
        }
    }
}