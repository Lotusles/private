/**
 * Zorgt er voor dat een formulier niet per ongeluk door enter kan worden verzonden
 */

function preventEnter(Event){
    var Event = (Event) ? Event : ((Event) ? Event : null);
    var trigger = (Event.target) ? Event.target : ((Event.srcElement) ? Event.srcElement : null);

    if((Event.keyCode === 13) && (trigger.type==="text")){ return false;}
    document.onkeypress = preventEnter();
}