<?php
/**
 * Created by PhpStorm.
 * User: Lesley Forn
 * Date: 03/12/2017
 * Time: 19:34
 *
 * Recept 90:
 * Kalender v1.0
 *
 * Uitleg:
 * Genereer een HTML tabel met kolommen voor de zeven weekdagen en rijen voor de weken.
 * Hierbij gebruik je verschillende vormen van de datum en tijdfuncties date() en mktime().
 *
 */

// Tijdzone voor Nederland
date_default_timezone_set('Europe/Amsterdam');

// Huidige datum en tijd als een integer
$iDateTime = time();

// Aparte strinvariabelen voor de dag, de maand en het jaar.
$sDay = date('d', $iDateTime);
$sMonth = date('m', $iDateTime);
$sYear = date('Y', $iDateTime);


// eerste dag van de maand
$iFirstDayOfMonth = mktime(0,0,0, $sMonth, 1, $sYear);



/**
 * Title of the calender
 *
 * This procedure generates a string with a month name and its year.
 * With date('F') we get the long month names in English.
 *
 * Optional:
 * For operating systems without dutch as default language we could pass an array with the corresponding dutch month names.
 *
 */

$sCalenderHeader = date('F', $iFirstDayOfMonth);
$aENG = array( 'January','February','March','April','May','June','July','August','September','October','November','December');
$aNL = array('Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December');
$sCalenderHeader = str_replace($aENG, $aNL, $sCalenderHeader);
$sCalenderHeader .= ' '. $sYear;
unset($aENG, $aNL);



// Bepalen met welke weekdag de maand begint
$sFirstDayOfWeek = date('D', $iFirstDayOfMonth);

// Aantal legen cellen in de eerste week
switch($sFirstDayOfWeek){
    case 'Mon':
        $iEmptyCells = 0;
        break;

    case 'Tue':
        $iEmptyCells = 1;
        break;

    case 'Wed':
        $iEmptyCells = 2;
        break;

    case 'Thu':
        $iEmptyCells = 3;
        break;

    case 'Fri':
        $iEmptyCells = 4;
        break;

    case 'Sat':
        $iEmptyCells = 5;
        break;

    case 'Sun':
        $iEmptyCells = 6;
        break;
}

// Aantal dagen in de huidige maand (28 t/m 31)
$iDaysInCurrentMonth = cal_days_in_month(CAL_GREGORIAN, $sMonth, $sYear);

// Begin van tabel in (X)HTML
echo '<table class="Calender">';
echo '<thead>';
echo '<tr><th colspan="7">'. $sCalenderHeader.' </th></tr>';
echo '<tr>';
echo '<th><abbr title="maandag">Ma</abbr></th>';
echo '<th><abbr title="dinsdag">Di</abbr></th>';
echo '<th><abbr title="woensdag">Wo</abbr></th>';
echo '<th><abbr title="donderdag">Do</abbr></th>';
echo '<th><abbr title="vrijdag">Vr</abbr></th>';
echo '<th class="weekend"><abbr title="zaterdag">Za</abbr></th>';
echo '<th class="weekend"><abbr title="zondag">Zo</abbr></th>';
echo '</tr>';
echo '</thead>';

// Weken en Dagen
echo '<tbody>';
echo '<tr>';

// Dagen van de week tellen van 1 t/m 7
$iWeekday = 1;

// Aantal legen cellen toevoegen
while ($iEmptyCells > 0){
    echo '<td>&nbsp;</td>';
    $iEmptyCells = $iEmptyCells - 1;
    $iWeekday++;
}

// Dag van de maand 1 t/m 31
$iDayNumber = 1;

// Alle dagen in de maand voor weergaven
while($iDayNumber <= $iDaysInCurrentMonth){

    if ($iWeekday > 5){
        echo '<td class="weekend"> '. $iDayNumber .' </td>';
    }else{
        echo '<td> '. $iDayNumber .' </td>';
    }

    $iDayNumber++;
    $iWeekday++;

    // Na 7 dagen een nieuwe rij beginnen voor een nieuwe week
    if ($iWeekday > 7){
        echo '</tr><tr>';
        $iWeekday = 1;
    }
}

// Einde van de tabel afmaken met legen cellen
while ($iWeekday > 1 && $iWeekday <= 7){
    echo '<td>&nbsp;</td>';
    $iWeekday++;
}

echo '</tr>';
echo '</tbody>';
echo '</table>';