<?php
/**
 * Created by PhpStorm.
 * User: Lesley Forn
 * Date: 03/12/2017
 * Time: 21:08
 */


/**
 * General Functions
 */
function strip_entry($input_value){
    return strip_tags(trim(htmlentities($input_value)));
}

/**
 *
 * Berichten systeem via een txt file
 */
// Tijdzone voor Nederland
date_default_timezone_set('Europe/Amsterdam');

// Constante voor de bestandsnaam
define('BESTAND', 'reacties.htm');

// Formulier verwerken
if(strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){

    // Naam van de gebruiker verwerken
    if(isset($_POST['name'])){

        $sName = strip_entry($_POST['name']);

        if (strlen($sName) == 0){
            $sName = 'Anoniem';
        }
    }else{
        $sName = 'Anoniem';
    }

    // Reactie van de gebruiker verwerken
    if (isset($_POST['reaction'])){
        $sReaction = strip_entry($_POST['reaction']);

        if( strlen($sReaction) > 1){

            /**
             * Naam, datum en tijd toevoegen aan de reactie en opmaken als een alinea (x)html.
             * Regeleinde worden met nl2br() vervangen door <br />.
             *
             * @link http://www.php.net/manual/en/function.nl2br.php
             */

            $sReaction = htmlentities($sReaction);
            $sReaction = nl2br($sReaction);
            $sReaction  = '<p><strong>' . $sName
                . ' (post arrived ' . date('d-m-Y')
                . ' at: '. date('H:i') . ' uur): </strong><br />'
                . $sReaction . "</p>\r\n";

            // Bestand inhoud lezen als het bestand al bestaat
            if(file_exists(BESTAND)){

                // Bestand openen voor lezen
                $rHandle = fopen(BESTAND, 'r');
                // Alle bestaande inhoud van het bestand lezen
                $sFileContent = fread($rHandle, filesize(BESTAND));
                // Bestand sluiten
                fclose($rHandle);
            }else{
                $sFileContent = '';
            }

            /**
             * Nieuwe reacties toevoegen aan het BEGIN van de
             * bestaande inhoud, zodat de laatste reactie van boven staat
             */
            $sFileContent = $sReaction . $sFileContent;
            // Bestand openen voor schrijven
            $rHandle = fopen(BESTAND, 'w');
            // Bestand vergrendelen voor exclusieve toegang
            flock($rHandle, LOCK_EX);
            // Nieuwe bestandsinhoud opslaan
            fwrite($rHandle, $sFileContent);
            // Bestand vergrendeling opheffen
            flock($rHandle, LOCK_UN);
            // Bestand sluiten
            fclose($rHandle);
            // Variabelen opruimen
            unset($sReaction, $rHandle, $sFileContent);
        }
    }
}

/*********************** Begin van weergaven html ************************************/

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h2>Reacties</h2>
<form action="" method="POST">
    <p>
        <label for="name">Name (Not Required):</label>
        <input id="name" name="name" type="text">
    </p>
    <p>
        <label for="reaction">You may enter your reaction, question or message here:</label>
        <br />
        <textarea name="reaction" id="reaction" cols="45" rows="10"></textarea>
    </p>
    <p>
        <input type="submit" value="Send Message" />
    </p>
</form>

<?php
/**
 * Bestand net de reacties insluiten als dat bestaat
 * en anders melding met een uitnodiging weergeven.
 */

if (file_exists(BESTAND)){
    include BESTAND;
}else{
    echo '<p> <strong>There are no messages post yet</strong> </p>';
}

?>

</body>
</html>
